package postgres

import (
	"context"
	"final.net/final/pkg/models"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"strconv"
	"time"
)

const (
	getIsSlotIsFree 			= "select id from booking where id=$1 expired > $2 AND booking_date < $2"
	addNewBooking 				= "INSERT INTO booking (user_id,table_id,status,created,booking_date,expired) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id"
	getAllTablesInfoByDate 		= "SELECT t.id,t.place_number,t.position, COALESCE((SELECT status from booking inner join tables on booking.table_id = tables.id and t.id = booking.table_id where booking_date <= $1 AND expired > $1 limit 1),'free') status from tables t left join booking b on t.id = b.table_id group by t.id,t.place_number,t.position order by t.position"
	insertSql                   = "INSERT INTO snippets (title,content,created,expires) VALUES ($1,$2,$3,$4) RETURNING id"
	getSnippetById              = "SELECT id, title, content, created, expires FROM snippets WHERE id=$1 AND expires > now() "
	getLatestTenCreatedSnippets = "SELECT id, title, content, created, expires FROM snippets WHERE expires > now() ORDER BY created DESC LIMIT 10"
)

type SnippetModel struct {
	Pool *pgxpool.Pool
}

type BookingModel struct {
	Pool *pgxpool.Pool
}

func (m *BookingModel) Insert(userId , tableId , date string) (int, error) {
	var id uint64
	uid,err := strconv.Atoi(userId)
	if err!=nil{
		return 0, err
	}
	tid,err := strconv.Atoi(tableId)
	if err!=nil{
		return 0, err
	}
	newDate,err := time.Parse("02-01-2006",date)
	if err != nil {
		fmt.Println(err)
		return 0, err
	}

	endDate := newDate.AddDate(0,0,1)
	end := endDate.Format("02-01-2006")
	timeNow := time.Now().Format("02-01-2006")
	row := m.Pool.QueryRow(context.Background(), addNewBooking, uid, tid,"new", timeNow,date,end)
	err = row.Scan(&id)
	if err != nil {
		return 0, err
	}
	return int(id), nil
}

func (m *SnippetModel) Insert(title, content, expires string) (int, error) {
	var id uint64
	interval, err := strconv.Atoi(expires)
	row := m.Pool.QueryRow(context.Background(), insertSql, title, content, time.Now(), time.Now().
		AddDate(0, 0, interval))
	err = row.Scan(&id)
	if err != nil {
		return 0, err
	}
	return int(id), nil
}

func (m *BookingModel) GetIsSlotIsFree(id int,checkingTime string) (bool, error) {
	b := &models.Booking{}
	err := m.Pool.QueryRow(context.Background(), getIsSlotIsFree, id, checkingTime).
		Scan(&b.ID)
	if err != nil {
		if err.Error() == "no rows in result set" {
			return false, models.ErrNoRecord
		} else {
			return false, err
		}
	}
	return true,nil
}

func (m *BookingModel) GetAllTablesInfoByDate(checkingTime string) ([]*models.BookingTable, error){
	fmt.Println("before query", checkingTime)
	rows, err := m.Pool.Query(context.Background(), getAllTablesInfoByDate, checkingTime)
	if err != nil {
		return nil,err
	}
	defer rows.Close()
	tables := []*models.BookingTable{}
	for rows.Next() {
		t := &models.BookingTable{}
		err = rows.Scan(&t.ID, &t.PlaceNumber, &t.Position,&t.Status)
		if err != nil {
			fmt.Println("ошибка вот здесь!")
			return nil,err
		}
		// Append it to the slice of snippets.
		tables = append(tables, t)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return tables, nil
}

func (m *SnippetModel) Get(id int) (*models.Snippet, error) {
	s := &models.Snippet{}
	err := m.Pool.QueryRow(context.Background(), getSnippetById, id).
		Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
	if err != nil {
		if err.Error() == "no rows in result set" {
			return nil, models.ErrNoRecord
		} else {
			return nil, err
		}
	}
	return s, nil
}

func (m *SnippetModel) Latest() ([]*models.Snippet, error) {
	rows, err := m.Pool.Query(context.Background(), getLatestTenCreatedSnippets)
	if err != nil {
		return nil, err
	}

	snippets := []*models.Snippet{}

	defer rows.Close()

	for rows.Next() {

		s := &models.Snippet{}

		err = rows.Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
		if err != nil {
			return nil, err
		}
		// Append it to the slice of snippets.
		snippets = append(snippets, s)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return snippets, nil
}
